import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class McDonaldsService {
  private host="http://localhost:8080";
  constructor(private http: HttpClient) { }

  search(code) {
    return this.http.get(this.host+'/mcdonalds?code='+code);
  }
}