import { Component } from '@angular/core';
import { McDonaldsService } from './mcdonalds.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public mcDonalds;
  public word="";
  title = 'app';
  constructor(private mcDonaldsService:McDonaldsService){

  }
onChange(event){
  this.word=event.target.value;
this.mcDonaldsService.search(this.word).subscribe(res=>{
  this.mcDonalds=res;
})
}
}
